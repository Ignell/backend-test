const fs = require('fs')



function generateCollection(collection, user, cron, social) { // sukuriamas collection object Icollection
    return {
        _id: `ObjectId('${collection}')`,
        userId: `ObjectId('${user}')`,
        cronList: cron,
        socialUnitList: social
    }
}




// id

function generateId(whatToGenerate, lastNumbers) { // 24 sk id 1 + 19 + 4
    let fullId = [];
    let firstNum;

    if (whatToGenerate == 'collection') {
        firstNum = 1;
        fullId.push(firstNum); // pirmas sk 
    } else if (whatToGenerate == 'user') {
        firstNum = 2;
        fullId.push(firstNum); // pirmas sk 
    } else if (whatToGenerate == 'name') {
        firstNum = 3;
        fullId.push(firstNum); // pirmas sk 
    } else if (whatToGenerate == 'cron') {
        firstNum = 4;
        fullId.push(firstNum); // pirmas sk 
    } else if (whatToGenerate == 'social') {
        firstNum = 5;
        fullId.push(firstNum); // pirmas sk 
    }

    if (firstNum < 10) {
        for (let i = 0; i < 23; i++) {
            fullId.push(0)
        }
    } else if (firstNum > 9) {
        for (let i = 0; i < 22; i++) {
            fullId.push(0)
        }
    }

    if (lastNumbers < 10) { // 4 pask sk
        fullId.pop();
        fullId.push(lastNumbers);
    } else if (lastNumbers > 9 && lastNumbers < 100) {
        fullId.pop()
        fullId.pop()
        fullId.push(lastNumbers);
    } else if (lastNumbers > 99 && lastNumbers < 1000) {
        fullId.pop()
        fullId.pop()
        fullId.pop()
        fullId.push(lastNumbers);
    } else if (lastNumbers > 999 && lastNumbers < 9999) {
        fullId.pop()
        fullId.pop()
        fullId.pop()
        fullId.pop()
        fullId.push(lastNumbers);
    }
    let id = fullId.join('');
    return id;
}










////////////////////////////////////////////////////////////////////////////

function create(usersCount, collectionCount, cronCount, socialCount) {
    let arr = [];

    let xArr = [];

    for (let x = 0; x < usersCount; x++) {
        var userId = generateId('user', x);
        for (let i = 0; i < collectionCount; i++) {
            var collectionId = generateId('collection', i);
        }
        for (let i = 0; i < socialCount; i++) {
            var socialList = generateSocial(i)
        }
        for (let i = 0; i < cronCount; i++) {
            var cronList = generateeCron(i)
            console.log(cronList[0])


            let postOnNumber = makeid(12);

            let postOn = new Date(parseInt(postOnNumber)).toISOString()

            let x = generateCron(cronList[i], 'content', 'imageUrl', postOn, collectionId)

            xArr.push(x);
            console.log(cronIdVal)
        }
        arr.push(generateCollection(collectionId, userId, cronList, socialList))
    }
    console.log(arr)
    console.log(xArr)






    let data = JSON.stringify(arr)
    fs.writeFileSync('my-data.json', data);
    return arr;
}

let cronIdVal = [];

function generateeCron(howMany) {
    let arr = []
    for (let y = 0; y < howMany; y++) {
        let cron = generateId('cron', y);
        cronIdVal.push(cron);

        arr.push("ObjectID('" + cron + "')")
    }
    return arr;
}

function generateSocial(howMany) {
    let arr = []
    for (let i = 0; i < howMany; i++) {
        let social = generateId('social', i);
        arr.push("ObjectID('" + social + "')")
    }
    return arr
}



const numberOfObjectsToGenerate = {
    cron: 3,
    users: 1,
    socialNetworks: 3,
    collections: 1,
}


create(numberOfObjectsToGenerate.users, numberOfObjectsToGenerate.collections, numberOfObjectsToGenerate.cron, numberOfObjectsToGenerate.socialNetworks)


///////////////////////////////////////////////////////////




function generateCron(id, content, imageUrl, postOn, collectionId) {
    return {
        _id: `${id}`,
        content: `${content}`,
        imageUrl: `${imageUrl}`,
        postOn: `${postOn}`,
        collectionId: `${collectionId}`
    }
}


// createCron(1, 1)


function createCron(cronId, collectionId) {
    let arr = [];
    let dummyContent = 'xxx';
    let dummyUrl = 'www.google.com';

    let postOnNumber = makeid(12);

    let postOn = new Date(parseInt(postOnNumber)).toISOString()




    let cronUnit = generateCron(cronId, dummyContent, dummyUrl, postOn, collectionId)
    arr.push(cronUnit)



    let data = JSON.stringify(arr)
    fs.writeFileSync('my-data.json', data);
    return arr;
}


function makeid(length) {
    var result = '';
    var characters = '0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}